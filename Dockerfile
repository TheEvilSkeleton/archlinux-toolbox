FROM docker.io/library/archlinux:base-devel

ENV NAME=archlinux VERSION=base-devel
LABEL com.github.containers.toolbox="true" \
      name="$NAME" \
      version="$VERSION" \
      usage="This image is meant to be used with the toolbox command" \
      summary="Base image for creating Arch Linux toolbox containers"

# Install extra packages
COPY extra-packages /
RUN pacman -Syu --needed --noconfirm - < extra-packages
RUN rm /extra-packages

# Clean up cache
RUN pacman -Scc --noconfirm

# Enable sudo permission for wheel users
RUN echo "%wheel ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/toolbox

# Allow host xdg commands
COPY xdg-commands.sh /
RUN install -Dm 777 xdg-commands.sh /etc/profile.d/xdg-commands.sh
RUN rm /xdg-commands.sh

CMD /bin/sh
